/* eslint-disable @typescript-eslint/no-explicit-any */
import { render } from "@testing-library/react";
import { Button, ThemeWrapper } from "../.";

const renderComponnent = (props?: any, value?: string) => {
  return render(
    <ThemeWrapper>
      <Button {...props}>{value}</Button>
    </ThemeWrapper>
  );
};

// create a describe block to test the Button component
describe("Button", () => {
  // test the Button component
  it("should render Button component", () => {
    const button = renderComponnent();
    expect(button.getByTestId("button-element")).toBeInTheDocument();
  });

  it("should render Button component with text", () => {
    const button = renderComponnent({}, "Button");
    expect(button.getByTestId("button-element")).toHaveTextContent("Button");
  });
});
