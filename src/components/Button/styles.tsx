import styled, { css } from "styled-components";

export const Container = styled.button`
  ${({ theme }) => css`
    outline: none;
    border: none;
    background-color: ${theme.colors.board[0]};
    color: white;
    padding: 0.75rem 1.5rem;
    border-radius: 0.25rem;
    min-width: 220px;
    transition: 140ms ease;
    font-size: 1rem;
    font-weight: 600;

    &:hover {
      cursor: pointer;
      background-color: ${theme.colors.secondary[0]};
      transition: 140ms ease;
      color: ${theme.colors.background[0]};
    }
  `}
`;
