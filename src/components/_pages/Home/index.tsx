import { Container, BoardContainer } from "./styles";
import { Board, Button } from "components";
import { useState } from "react";

export const Home = () => {
  const [play, setPlay] = useState(false);

  return (
    <Container>
      <BoardContainer>
        <Board setPlay={setPlay} play={play} />

        <Button onClick={() => setPlay((current) => !current)}>
          {play ? "Parar" : "Novo Jogo"}
        </Button>
      </BoardContainer>
    </Container>
  );
};
