import { Position } from "interfaces";
import styled, { css } from "styled-components";

interface FoodProps {
  position?: Position;
}

export const ContainerFood = styled.div<FoodProps>`
  ${({ theme, position }) => css`
    background-color: ${theme.colors.food[0]};
    grid-row-start: ${position?.y};
    grid-column-start: ${position?.x};
    width: auto;
    height: auto;
    position: relative;

    border-radius: 50%;
    transform: scale(0.7);
  `}
`;
