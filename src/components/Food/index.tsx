import { Position } from "interfaces";
import { ContainerFood } from "./styles";

interface FoodProps {
  position?: Position;
}

export const Food: React.VFC<FoodProps> = ({ position }) => {
  return <ContainerFood position={position} />;
};
