import { useEffect, useState } from "react";
import { Container, Header, Grid } from "./styles";

import { Food, Snake } from "components";
import {
  getInputDirection,
  getRandonFoodPosition,
  handleEatFood,
  hasCollided,
} from "utils";

const SNAKE_SPEED = 5;
const EXPANSION_RATE = 3;

const BOARD_SIZE = 50;
const SNAKE_BODY = [
  { x: 25, y: 28 },
  { x: 26, y: 28 },
  { x: 27, y: 28 },
];

interface BoardProps {
  play: boolean;
  setPlay: React.Dispatch<React.SetStateAction<boolean>>;
}

export const Board: React.VFC<BoardProps> = ({ play, setPlay }) => {
  const [velocity, setVelocity] = useState(0);
  const [seconds, setSeconds] = useState(0);
  const [points, setPoints] = useState(0);
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [snakeSpeed, setSnakeSpeed] = useState(SNAKE_SPEED);
  const [snakeBody, setSnakeBody] = useState(SNAKE_BODY);
  const [foodPosition, setFoodPosition] = useState({ x: 20, y: 20 });

  useEffect(() => {
    if (!play) return;
    setTimeout(() => {
      setVelocity((current) => current + 1);
      if (handleEatFood(foodPosition, snakeBody)) addNewSegment();
      if (hasCollided(snakeBody, BOARD_SIZE)) {
        setPlay(false);
        return;
      }
      updateMovement();
    }, 333 / snakeSpeed);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [velocity, play]);

  useEffect(() => {
    if (!play) return;
    setTimeout(() => {
      setSeconds((current) => current + 1);
    }, 1000);
  }, [play, seconds]);

  const updateMovement = () => {
    const inputDirection = getInputDirection();
    setSnakeBody((current) =>
      current.map((segment, index) => {
        if (index === 0) {
          return {
            x: segment.x + inputDirection.x,
            y: segment.y + inputDirection.y,
          };
        } else {
          return {
            x: current[index - 1].x,
            y: current[index - 1].y,
          };
        }
      })
    );
  };

  const addNewSegment = () => {
    setPoints((current) => current + 1);
    setFoodPosition(getRandonFoodPosition(snakeBody, BOARD_SIZE));
    for (let i = 0; i < EXPANSION_RATE; i++) {
      setSnakeBody((current) => current.concat(current[current.length - 1]));
    }
  };

  return (
    <Container>
      <Header>
        <span>{points} pontos</span>
        <span>velocidade: {snakeSpeed}</span>
        <span>tempo: {seconds} segundos</span>
      </Header>
      <Grid size={BOARD_SIZE}>
        <Snake body={snakeBody} />
        <Food position={foodPosition} />
      </Grid>
    </Container>
  );
};
