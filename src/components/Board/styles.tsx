import styled, { css } from "styled-components";

export const Container = styled.div`
  ${({ theme }) => css`
    display: flex;
    flex-direction: column;
    background-color: ${theme.colors.board[0]};
    width: 95vmin;
    height: 95vmin;
    max-width: 600px;
    max-height: 600px;
    border-radius: 1rem;
  `}
`;

interface GridProps {
  size: number;
}

export const Grid = styled.div<GridProps>`
  ${({ size }) => css`
    display: grid;
    grid-template-columns: repeat(${size}, 1fr);
    grid-template-rows: repeat(${size}, 1fr);
    width: 100%;
    height: 100%;
    position: relative;
  `}
`;
export const Header = styled.div`
  ${({ theme }) => css`
    display: flex;
    height: 60px;
    width: 100%;
    align-items: center;
    padding: 0 1.5rem;
    color: white;
    background-color: ${theme.colors.board[1]};
    border-radius: 1rem 1rem 0 0;

    span {
      margin-left: 1rem;
      text-transform: uppercase;
    }
  `}
`;
