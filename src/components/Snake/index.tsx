import { Position } from "interfaces";
import { ContainerSnake } from "./styles";

interface SnakeProps {
  body: Position[];
}

export const Snake: React.VFC<SnakeProps> = ({ body }) => {
  return (
    <>
      {body.map((position, index) => (
        <ContainerSnake key={index} position={position} />
      ))}
    </>
  );
};
