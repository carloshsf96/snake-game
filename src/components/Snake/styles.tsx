import { Position } from "interfaces";
import styled, { css } from "styled-components";

interface SnakeProps {
  position?: Position;
}

// export const ContainerSnake = styled.div<SnakeProps>`
//   ${({ theme, position }) => css`
//     background-color: ${theme.colors.snake[0]};
//     grid-row-start: ${position?.y};
//     grid-column-start: ${position?.x};
//     width: auto;
//     height: auto;
//     position: relative;
//     z-index: 2;
//     box-shadow: 0 0 15px 1px rgba(12, 141, 87, 0.568);
//   `}
// `;

export const ContainerSnake = styled.div.attrs<SnakeProps>(({ position }) => ({
  style: {
    gridRowStart: position?.y,
    gridColumnStart: position?.x,
  },
}))<SnakeProps>`
  ${({ theme }) => css`
    background-color: ${theme.colors.snake[0]};
    width: auto;
    height: auto;
    position: relative;
    z-index: 2;
    box-shadow: 0 0 15px 1px rgba(12, 141, 87, 0.568);
  `}
`;
