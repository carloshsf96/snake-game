export const defaultTheme = {
  colors: {
    primary: ["#fafafa", "#0c71ab"],
    secondary: ["#B1D4E0", "#ebf4f7"],
    background: ["#141414", "#062036"],
    board: ["#383838", "#202020"],
    food: ["#edbe5f"],
    snake: ["#16a818"],
  },
  grid: {
    container: ["1024px", "768px", "576px", "320px"],
  },
};
