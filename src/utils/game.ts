import { Position } from "interfaces";

export const handleEatFood = (food: Position, snake: Position[]) => {
  return snake[0].x === food.x && snake[0].y === food.y;
};

export const isOverlapping = (food: Position, snake: Position[]) => {
  return snake.some((segment) => segment.x === food.x && segment.y === food.y);
};

export const between = (min: number, max: number) => {
  return Math.floor(Math.random() * (max - min) + min);
};

export const generateRandonPosition = (size: number): Position => {
  return {
    x: between(1, size),
    y: between(1, size),
  };
};

export const getRandonFoodPosition = (snake: Position[], size: number) => {
  let food: Position;
  do {
    food = generateRandonPosition(size);
  } while (isOverlapping(food, snake));
  return food;
};

export const hasCollided = (snake: Position[], boardSize: number) => {
  const head = snake[0];
  const rest = snake.slice(1);
  const snakeX = head.x;
  const snakeY = head.y;
  const collision =
    rest.some((segment) => segment.x === snakeX && segment.y === snakeY) ||
    snakeX <= 0 ||
    snakeX >= boardSize ||
    snakeY <= 0 ||
    snakeY >= boardSize + 1;

  // if (collision) setSnakeBody(rest);

  return (
    collision ||
    snakeX <= 0 ||
    snakeX >= boardSize ||
    snakeY <= 0 ||
    snakeY >= boardSize + 1
  );
};